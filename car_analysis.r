# <NAME>
# TP<123456>

#import lib
library(dplyr)
library(ggplot2)

#1. data import
raw_data = read.csv(file = "data.csv",encoding ="UTF-8", header = TRUE)

#1.1 data clean
is_data_duplicated = duplicated.data.frame(raw_data)
summary(is_data_duplicated) # check whether data is duplicated 
data_unique <- raw_data[!duplicated(raw_data),] # remove duplicated rows

#1.2 pre-processing
data = data_unique
str(data) # check the attributes' data types of the dataset
summary(data) # summary data on each attribute of dataset 
sapply(data, function(x) sum(is.na(x))) # summary of NA in each col of data
data[is.na(data)]= 0 # replace all NA to value 0
View(data)

# Data Manipulation

#2 single value
#2.1 single value : CO2
ggplot(data,aes(x = co2)) + geom_histogram() 
ggplot(data,aes(x = co2)) + geom_density() 
ggplot(data,aes(y = co2, x = 1)) + geom_boxplot() 

#2.2 single value : co
ggplot(data,aes(x = co_emissions)) + geom_histogram() 
ggplot(data,aes(x = co_emissions)) + geom_density(lwd=1) 
ggplot(data,aes(y=co_emissions, x=1)) + geom_boxplot() 

#2.3 single value : fuel_cost_6K_miles
cost_6k_tbl = filter(data, fuel_cost_6000_miles != 0) 

ggplot(cost_6k_tbl,aes(x = fuel_cost_6000_miles)) + geom_histogram() 
ggplot(cost_6k_tbl,aes(x = fuel_cost_6000_miles)) + geom_density() 
ggplot(cost_6k_tbl,aes(y=fuel_cost_6000_miles, x=1)) + geom_boxplot() 

#2.4 single value : fuel_cost_12K_miles
cost_12k_tbl = filter(data, fuel_cost_12000_miles != 0) 

ggplot(cost_12k_tbl,aes(x = fuel_cost_12000_miles)) + geom_histogram() 
ggplot(cost_12k_tbl,aes(x = fuel_cost_12000_miles)) + geom_density() 
ggplot(cost_12k_tbl,aes(y=fuel_cost_12000_miles, x=1)) + geom_boxplot() 

# 3 bivariate relationship
# 3.1 summary manufacturer of car
library(stringr)
pos <- regexpr("-",data$Maker)
manufacturer_col <- substr(data$Maker, 1,pos-1)
data2 = transform(data,manufacturer_col=manufacturer_col)
View(data2)

manufacturer_group = data2  %>%
  select(c("year","manufacturer_col"))%>%
  group_by(manufacturer_col)%>%
  summarise(total = n())
View(manufacturer_group)

install.packages("RColorBrewer")
require("RColorBrewer")
# calculate percent of car manufacturer
piepercent = manufacturer_group$total/sum(manufacturer_group$total)
piepercent = paste(round(100 * piepercent,2),"%")
# draw pie plot 
pie(manufacturer_group$total,
    labels = paste(levels(factor(manufacturer_col)),'(',as.character(piepercent),')'),
    main = 'Share of manufacturer',col=brewer.pal(length(manufacturer_group$total),"Spectral"))

# 3.2  euro_standard ~ fuel_cost
cost_6k_tbl = data %>%
  filter(fuel_cost_6000_miles != 0) %>%
  select(c("fuel_cost_6000_miles","euro_standard")) 

ggplot(cost_6k_tbl, aes(y=fuel_cost_6000_miles, x=factor(euro_standard))) + 
  geom_boxplot()+
  labs(title = 'Boxplot of fuel_cost_6k_miles against euro_standard',x = "Euro Standard")

cost_12k_tbl = data %>%
  filter(fuel_cost_12000_miles != 0) %>%
  select(c("fuel_cost_12000_miles","euro_standard"))

ggplot(cost_12k_tbl, aes(y=fuel_cost_12000_miles, x=factor(euro_standard))) + 
  geom_boxplot() +
  labs(title = 'Boxplot of fuel_cost_12k_miles against euro_standard',x = "Euro Standard")


# 3.3 engine_capacity ~ CO2
engine_capacity_and_co2 = data %>%
  select(c("engine_capacity","co2"))

# Draw scatter plot of engine capacity against CO2
ggplot(engine_capacity_and_co2,aes(x=engine_capacity,y=co2))+
  geom_point(alpha=0.4)+ 
  stat_smooth(method = "lm", formula = y ~ x,se=F) +
  labs(title = 'Scatter Plot of engine capacity against CO2')

  func <- engine_capacity_and_co2[,"co2"] ~ engine_capacity_and_co2[,"engine_capacity"]
  model <- lm(func,engine_capacity_and_co2)
  summary(model)
  # CO2 = 0.05058 * engine_capacity + 79.21491

# 3.4 fuel_type ~ CO2 
fuel_types = levels(factor(data$fuel_type))
View(fuel_types) 
data %>%
  group_by(fuel_type) %>%
  summarize(total = n())

fuel_types_and_co2 = data %>%
  select(c("fuel_type","co2"))%>%
  filter(  fuel_type == "Diesel" |
             fuel_type == "Petrol" |
             fuel_type ==  "Electricity" |
             fuel_type ==  "LPG " | fuel_type ==  "CNG")

ggplot(fuel_types_and_co2, aes(y=co2, x=factor(fuel_type))) + 
  geom_boxplot()+
  stat_summary(fun=mean, colour="green", geom="point", shape=17, size=2,show_guide = FALSE)+
  labs(title = 'Boxplot of co2 against fuel_type',x = "fuel_type")

#3.5  fuel_type ~ CO
fuel_types_and_co = data %>%
  select(c("fuel_type","co_emissions"))%>%
  filter(  fuel_type == "Diesel" |
             fuel_type == "Petrol" |
             fuel_type ==  "Electricity" |
             fuel_type ==  "LPG " | fuel_type ==  "CNG")

ggplot(fuel_types_and_co, aes(y=co_emissions, x=factor(fuel_type))) + 
  geom_boxplot()+
  labs(title = 'Boxplot of co emissions against fuel type',x = "fuel_type")

#3.6 summarize the mean of co2 and fuel cost 
#    with different tax bands for car.  
data %>%
  filter(tax_band!="" & tax_band!="N/A") %>%
  select(c("year","tax_band","co2","fuel_cost_12000_miles")) %>%
  group_by(tax_band)  %>%
  summarise(total = n(),co2_mean = mean(co2),fuel_cost_mean=mean(fuel_cost_12000_miles))

#3.7 CO2 emissions by year
ggplot(data,aes(x = co2)) +
  geom_histogram() +
  labs(title = 'CO2 emissions histogram by year',x = 'CO2')+
  facet_wrap(~year)


